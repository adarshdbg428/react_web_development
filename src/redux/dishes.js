import * as ActionTypes from './ActionTypes';

export const Dishes = (state = { isLoading: true,
    errMess: null,
    dishes:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_DISHES:
            return {...state, isLoading: false, errMess: null, dishes: action.payload};

        case ActionTypes.DISHES_LOADING:
            return {...state, isLoading: true, errMess: null, dishes: []}

        case ActionTypes.DISHES_FAILED:
            return {...state, isLoading: false, errMess: action.payload};
         case ActionTypes.ADD_COMMENT:
                var comment = action.payload;
                comment.id = state.length;
                comment.date = new Date().toISOString();
                console.log("Comment: ", comment);
                return {...state,comments:state.comment.concat(Comment)};

        default:
            return state;
    }
};